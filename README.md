# Amassment Task for Frontend Role at Chooose Company

## Project Description

This project is an assessment task for the frontend role at Chooose Company. It aims to evaluate the skills and capabilities of potential candidates for the position.

## Getting Started

These instructions will help you set up the project on your local machine for development and testing purposes.

### Prerequisites

You need Node.js version 18 or higher and npm installed on your machine.

### Installing

Clone the repository:

```bash
git clone git@gitlab.com:kkorczyn/chooose.git
```

Install the dependencies:

```bash
npm install
```

## Running the Application

To start the server, run the following command:

```bash
 npm run start:server
```

To start the frontend application, use the following command:

```bash
npm start
```

## Built With

- [Node.js](https://nodejs.org/)
- [TypeScript](https://www.typescriptlang.org/)

## Author

**Kamil Korczynski**

## License

This project is licensed under the MIT License.
