import { Box, Card, CardBody, HStack, Heading, Text } from '@chakra-ui/react';
import { StarMeter } from '../StarMeter/StarMeter';
import { calculateEmission } from '../../helpers/calculateEmission';

interface TripCardProps {
  countries: number;
  days: number;
  emission: number;
  imageUrl: string;
  name: string;
  rating: number;
}

export const TripCard = (props: TripCardProps) => {
  const { countries, days, name, imageUrl, emission, rating } = props;

  return (
    <Card
      flexShrink={1}
      h="xs"
      m={2}
      p={2}
      position="relative"
      rounded="xl"
      variant="elevated"
      w="sm"
    >
      <CardBody
        alignItems="center"
        backgroundImage={`linear-gradient(
          rgba(0, 0, 0, 0.4),
          rgba(0, 0, 0, 0.1)
        ),url(${imageUrl})`}
        backgroundPosition="bottom"
        data-testid="card-body"
        display="flex"
        flexDirection="column"
        p={0}
        rounded="lg"
      >
        <Box textAlign="center" mt={20}>
          <Heading size="lg" color="white">
            {name}
          </Heading>
          <Text fontSize="md" color="white">
            {/* singular / plural words could be handled with i18n */}
            {countries} {countries === 1 ? 'country' : 'countries'}, {days}
            {days === 1 ? ' day' : ' days'}.
          </Text>
        </Box>

        <HStack
          bgColor="#333333"
          color="white"
          justifyContent="space-between"
          maxW="100%"
          mt={4}
          p={4}
          rounded="lg"
          w="xs"
        >
          <Text>Emission offset:</Text>
          <Text>
            {calculateEmission(emission)} CO<sub>2</sub>e
          </Text>
        </HStack>

        <HStack
          bgColor="white"
          bottom={2}
          maxW="100%"
          p={2}
          pl={4}
          position="absolute"
          pr={4}
          roundedTop="md"
          w="xs"
        >
          <Text mr="auto">Trip rating:</Text>
          <StarMeter rating={rating} />
          <Text fontSize="lg" fontWeight={700} data-testid="rating">
            {rating.toFixed(1)}
          </Text>
        </HStack>
      </CardBody>
    </Card>
  );
};
