import { render, screen } from '@testing-library/react';
import { TripCard } from '../TripCard';

const mockTrip = {
  countries: 1,
  days: 2,
  emission: 810,
  rating: 4,
  name: 'European Quest',
  imageUrl: 'someUrl',
};

describe('TripCard', () => {
  test('should render all data', () => {
    render(<TripCard {...mockTrip} />);

    expect(screen.getByText('European Quest')).toBeInTheDocument();
    expect(screen.getByTestId('card-body')).toHaveStyle(
      'background-image: linear-gradient(rgba(0, 0, 0, 0.4), rgba(0, 0, 0, 0.1)),url(someUrl);'
    );
    expect(screen.getByText(/1\scountry/)).toBeInTheDocument();
    expect(screen.getByText(/2\sdays/)).toBeInTheDocument();
    expect(screen.getByText(/810 kg\sCO/)).toBeInTheDocument();
    expect(screen.getByTestId('rating')).toHaveTextContent('4.0');
  });
});
