import styled from '@emotion/styled';
import { useMemo } from 'react';

const StyledMeter = styled.div<{ percent: string }>`
  color: black;
  flex: 0 0 auto;
  font-size: 1.5em;
  position: relative;

  &::before {
    content: '★★★★★';
    opacity: 0.3;
  }

  &::after {
    color: gold;
    content: '★★★★★';
    left: 0;
    overflow: hidden;
    position: absolute;
    top: 0;
    white-space: nowrap;
    width: ${({ percent }) => percent}};
    z-index: 1;
  }
`;

interface StarMeterProps {
  rating: number;
}

export const StarMeter = ({ rating }: StarMeterProps) => {
  const percent = useMemo(() => `${(rating / 5) * 100}%`, [rating]);

  return <StyledMeter percent={percent} />;
};
