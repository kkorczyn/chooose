import * as React from 'react';

import { ChakraProvider } from '@chakra-ui/react';
import { Board } from './views/Board';

function App() {
  return (
    <ChakraProvider>
      <Board />
    </ChakraProvider>
  );
}

export default App;
