import { Container } from '@chakra-ui/react';
import React, { useEffect, useState } from 'react';
import { TripCard } from '../components/TripCard/TripCard';

interface Trip {
  id: number;
  countries: number;
  days: number;
  imageUrl: string;
  name:  string;
  emission: number;
  rating: number;
}

export const Board = () => {
  const [trips, setTrips] = useState<Trip[]>([]);

  useEffect(() => {
    const getTrips = async () => {
      const response = await fetch('http://localhost:3001/trips');
      const data: Trip[] = await response.json();
      setTrips(data);
    };
    getTrips();
  }, []);

  return (
    <Container
      bg="#dddddd"
      maxW="100%"
      h="100vh"
      p={4}
      display="flex"
      flexWrap="wrap"
      justifyContent="center"
    >
      {trips.map((trip) => (
        <TripCard
          key={trip.id}
          countries={trip.countries}
          days={trip.days}
          imageUrl={trip.imageUrl}
          name={trip.name}
          emission={trip.emission}
          rating={trip.rating}
        />
      ))}
    </Container>
  );
};
