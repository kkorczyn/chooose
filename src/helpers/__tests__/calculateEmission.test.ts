import { calculateEmission } from '../calculateEmission';

describe('calculateEmission', () => {
  test('returns correctly formatted value', () => {
    expect(calculateEmission(1000)).toBe('1.00 t');
    expect(calculateEmission(999)).toBe('999 kg');
    expect(calculateEmission(4563)).toBe('4.56 t');
    expect(calculateEmission(4567)).toBe('4.57 t');
  });
});
