export const calculateEmission = (emission: number): string => {
  if (emission < 1000) {
    return `${emission} kg`;
  } else {
    return `${(emission / 1000).toFixed(2)} t`;
  }
};
